## ROI Calculator

A ROI (return on investment) calculator in the form of a wizard. The user-entered form values are sent to the server and got back to the client to perform some calculations and display the results in tables.

The project was created in React.js (+ React Router) and using Firebase and RWD. The app features my own Webpack configuration to be able to: load different types of files, minify CSS and JS, use React.js, ES6 (Babel), Sass, autoprefixer, ES Lint, Prettier.

Installing project dependencies:  
`npm install`

Running a development server:  
`npm start`

Generating a production build:  
`npm run build`

See the project [here](http://sandramichalska.pl/roi-calculator/)
