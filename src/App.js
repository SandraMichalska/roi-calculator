import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Welcome from './components/Welcome/Welcome';
import Form from './containers/Form/Form';
import Results from './components/Results/Results/Results';
import ResultsMore from './components/Results/ResultsMore/ResultsMore';

class App extends React.Component {
    render() {
        return (
            <Switch>
                <Route path='/form' component={Form} />
                <Route path='/results' component={Results} />
                <Route path='/results-more' component={ResultsMore} />
                <Route exact path='/' component={Welcome} />
            </Switch>
        );
    }
}

export default App;
