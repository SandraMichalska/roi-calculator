import React from 'react';
import { Route, Link } from 'react-router-dom';

import FormStep from '../../components/FormStep/FormStep';
import Radios from '../../components/UI/Radios/Radios/Radios';
import Field from '../../components/UI/Field/Field';
import Back from '../../components/UI/Back/Back';
import Tooltip from '../../components/UI/Tooltip/Tooltip';
import './Form.scss';

class Form extends React.Component {
    state = {
        formData: {
            quality: {
                selectedOption: 'mid',
            },
            ticketNumber: {
                value: '10000',
                validation: {
                    valid: true,
                    required: true,
                    minLength: 1,
                    maxLength: 15,
                },
            },
            ticketPrice: {
                value: '20',
                validation: {
                    valid: true,
                    required: true,
                    minLength: 1,
                    maxLength: 15,
                },
            },
        },
        isFormValid: true,
        loading: false,
        error: null,
    };

    disallowEInNumberField(inputIdentifier) {
        const inputField = document.getElementById(inputIdentifier);
        const invalidChars = ['e', 'E'];

        inputField.addEventListener('keydown', e => {
            if (invalidChars.includes(e.key)) e.preventDefault();
        });
    }

    checkValidity = inputData => {
        let isValid = true;

        if (inputData.validation.required) {
            isValid = inputData.value.trim() !== '' && isValid;
        }

        if (inputData.validation.minLength) {
            isValid =
                inputData.value.length >= inputData.validation.minLength &&
                isValid;
        }

        if (inputData.validation.maxLength) {
            isValid =
                inputData.value.length <= inputData.validation.maxLength &&
                isValid;
        }

        return isValid;
    };

    handleInputChange = (event, inputIdentifier, dataToChange) => {
        if (dataToChange === 'value') {
            this.disallowEInNumberField(inputIdentifier);
        }

        const updatedformData = {
            ...this.state.formData,
        };

        const updatedFormElement = {
            ...updatedformData[inputIdentifier],
        };

        updatedFormElement[dataToChange] = event.target.value;

        if (dataToChange === 'value') {
            updatedFormElement.validation.valid = this.checkValidity(
                updatedFormElement
            );
        }

        updatedformData[inputIdentifier] = updatedFormElement;

        let isFormValid = true;

        for (let identifier in updatedformData) {
            if (identifier === 'quality') continue;
            isFormValid =
                updatedformData[identifier].validation.valid && isFormValid;
        }

        this.setState({
            formData: updatedformData,
            isFormValid: isFormValid,
        });
    };

    handleContinue = () => {
        const currentPath = this.props.location.pathname;
        const currentFormStep = parseInt(currentPath.match(/\d+/g));
        let nextPath = `/form/${currentFormStep + 1}`;
        this.props.history.push(nextPath);
    };

    handleSubmit = e => {
        e.preventDefault();

        const formData = {
            streamingQuality: this.state.formData.quality.selectedOption,
            ticketNumber: this.state.formData.ticketNumber.value,
            ticketPrice: this.state.formData.ticketPrice.value,
        };

        this.setState({ loading: true });

        fetch(
            'https://live-ppv-event-roi-calculator.firebaseio.com/form-data.json',
            {
                method: 'post',
                body: JSON.stringify(formData),
            }
        )
            .then(response => {
                response.json().then(() => {
                    this.setState({ loading: false });
                    this.props.history.push('/results');
                });
            })
            .catch(err => {
                console.log('Fetch error:', err);
                this.setState({ error: err, loading: false });
            });
    };

    prepareFormSteps() {
        const formStepsData = [
            {
                txt1:
                    'Working with a Live PPV specialist can save you a lot of time and development costs.',
                icon: 'video',
                txt2:
                    'When you are building your monetization platform, you consider the costs of design, streaming, along with landing page and e-commerce checkout development.',
                question: 'What level of streaming quality are you aiming for?',
                tooltip: (
                    <Tooltip tip='This selection has an impact on the fixed costs and the conversion rate. A higher quality of service brings higher conversion rate.' />
                ),
                input: (
                    <Radios
                        selectedOption={
                            this.state.formData.quality.selectedOption
                        }
                        onInputChange={this.handleInputChange}
                    />
                ),
                buttonType: 'button',
                buttonValue: 'Next question',
            },
            {
                txt1:
                    'Working with a Live PPV specialist can boost your conversion rate and ultimately your ROI.',
                icon: 'chart-bar',
                question: 'How many tickets do you estimate to sell?',
                input: (
                    <Field
                        fieldId='ticketNumber'
                        value={this.state.formData.ticketNumber.value}
                        valid={
                            this.state.formData.ticketNumber.validation.valid
                        }
                        cls={
                            this.state.formData.ticketNumber.validation.valid
                                ? ''
                                : 'clc-field__input--error'
                        }
                        changed={() =>
                            this.handleInputChange(
                                event,
                                'ticketNumber',
                                'value'
                            )
                        }
                        disallowEInNumberField={this.disallowEInNumberField}
                    />
                ),
                buttonType: 'button',
                buttonValue: 'Next question',
                back: (
                    <Back
                        history={this.props.history}
                        cls='clc-back-btn--margin'
                    />
                ),
            },
            {
                txt1:
                    'Working with a Live PPV specialist can help you define your best pricing strategy.',
                icon: 'laptop',
                question:
                    'How much do you plan to charge per ticket for your Live PPV event?',
                input: (
                    <Field
                        fieldId='ticketPrice'
                        unit='€'
                        value={this.state.formData.ticketPrice.value}
                        valid={this.state.formData.ticketPrice.validation.valid}
                        cls={
                            this.state.formData.ticketPrice.validation.valid
                                ? ''
                                : 'clc-field__input--error'
                        }
                        changed={() =>
                            this.handleInputChange(
                                event,
                                'ticketPrice',
                                'value'
                            )
                        }
                        disallowEInNumberField={this.disallowEInNumberField}
                    />
                ),
                buttonType: 'submit',
                buttonValue: 'See the results',
                isFormValid: this.state.isFormValid,
                back: (
                    <Back
                        history={this.props.history}
                        cls='clc-back-btn--margin'
                    />
                ),
            },
            // add another object for a new form step
        ];

        const formStepsWithProgress = this.prepareProgressPercentages(
            formStepsData
        );

        return formStepsWithProgress;
    }

    prepareProgressPercentages(formStepsData) {
        let stepsProgress = [];
        const formStepsLength = formStepsData.length;

        let currentStep = 1;
        while (currentStep <= formStepsLength) {
            // set progress bar % depending on the current step
            const progress = Math.floor((100 / formStepsLength) * currentStep);
            stepsProgress.push(progress);
            currentStep++;
        }

        formStepsData.map((step, index) => {
            step.progress = stepsProgress[index];
        });

        return formStepsData;
    }

    prepareRoutes(formStepsLength, formStepsData) {
        const formRoutes = [];

        for (let i = 0; i < formStepsLength; i++) {
            formRoutes.push(
                <Route
                    path={`${this.props.match.path}/${i + 1}`}
                    render={() => (
                        <FormStep
                            stepsData={formStepsData[i]}
                            onContinue={() =>
                                formStepsData[i].buttonType !== 'submit'
                                    ? this.handleContinue(formStepsLength)
                                    : null
                            }
                        />
                    )}
                    key={i}
                />
            );
        }

        return formRoutes.map(route => route);
    }

    render() {
        if (this.state.loading) {
            return (
                <div className='clc-form'>
                    <p>Please wait a second...</p>
                </div>
            );
        }

        if (this.state.error) {
            return (
                <div className='clc-form'>
                    <p className='clc-form__error'>
                        An error occurred. Please try again later.
                    </p>
                    <Link to='/' className='clc-form__error-link'>
                        Go to the main page
                    </Link>
                </div>
            );
        }

        const formData = this.prepareFormSteps();

        return (
            <form onSubmit={this.handleSubmit}>
                <>{this.prepareRoutes(formData.length, formData)}</>
            </form>
        );
    }
}

export default Form;
