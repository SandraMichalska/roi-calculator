import 'normalize.css';
import './index.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';

import { library } from '@fortawesome/fontawesome-svg-core';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faVideo,
    faChartBar,
    faLaptop,
    faQuestionCircle,
} from '@fortawesome/free-solid-svg-icons';

import App from './App';

library.add(faVideo, faChartBar, faLaptop, faQuestionCircle);

ReactDOM.render(
    <Router>
        <App />
    </Router>,
    document.querySelector('.clc-app')
);
