import React from 'react';

import Button from '../Button/Button';
import './Back.scss';

class Back extends React.Component {
    handleGoBack = () => {
        this.props.history.goBack();
    };

    render() {
        return (
            <Button
                type='button'
                cls={`clc-back-btn ${this.props.cls}`}
                clicked={this.handleGoBack}>
                Back
            </Button>
        );
    }
}

export default Back;
