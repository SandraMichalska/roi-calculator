import React from 'react';
import { Redirect } from 'react-router-dom';

import Button from '../Button/Button';
import './StartAgain.scss';

class StartAgain extends React.Component {
    state = {
        redirect: false,
    };

    handleStartAgain = () => {
        this.setState({
            redirect: true,
        });
    };

    render() {
        let button = <Redirect to='/' />;

        if (!this.state.redirect) {
            button = (
                <Button
                    type='button'
                    cls='clc-restart-btn'
                    clicked={this.handleStartAgain}>
                    Start again
                </Button>
            );
        }

        return button;
    }
}

export default StartAgain;
