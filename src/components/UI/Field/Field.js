import React from 'react';

import './Field.scss';

class Field extends React.Component {
    componentDidMount() {
        this.props.disallowEInNumberField(this.props.fieldId);
    }

    render() {
        return (
            <div className='clc-field'>
                <span>
                    <input
                        id={this.props.fieldId}
                        className={`clc-field__input ${this.props.cls}`}
                        type='number'
                        name={this.props.fieldId}
                        value={this.props.value}
                        onChange={this.props.changed}
                    />
                    {this.props.unit}
                </span>
            </div>
        );
    }
}

export default Field;
