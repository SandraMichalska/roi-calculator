import React from 'react';

import Radio from '../Radio/Radio';
import './Radios.scss';

const Radios = props => {
    const radioButtons = [
        {
            id: 'quality-high',
            value: 'high',
            name: 'quality',
            label: 'High',
        },
        { id: 'quality-mid', value: 'mid', name: 'quality', label: 'Mid' },
        {
            id: 'quality-entry',
            value: 'entry',
            name: 'quality',
            label: 'Entry',
        },
    ];

    return (
        <div className='clc-radios'>
            {radioButtons.map(inputData => (
                <Radio
                    key={inputData.id}
                    id={inputData.id}
                    value={inputData.value}
                    label={inputData.label}
                    name={inputData.name}
                    checked={props.selectedOption === inputData.value}
                    changed={() =>
                        props.onInputChange(
                            event,
                            inputData.name,
                            'selectedOption'
                        )
                    }
                />
            ))}
        </div>
    );
};

export default Radios;
