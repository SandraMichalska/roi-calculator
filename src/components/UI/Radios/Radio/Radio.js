import React from 'react';
import './Radio.scss';

const Radio = props => (
    <div className='clc-radio'>
        <input
            id={props.id}
            className='clc-radio__quality'
            type='radio'
            name={props.name}
            value={props.value}
            checked={props.checked}
            onChange={props.changed}
        />
        <label className='clc-radio__lbl' htmlFor={props.id}>
            {props.label}
        </label>
    </div>
);

export default Radio;
