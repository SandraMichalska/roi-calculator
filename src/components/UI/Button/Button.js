import React from 'react';

import './Button.scss';

const Button = props => {
    return (
        <button
            type={props.type}
            disabled={props.isFormValid === false}
            onClick={props.clicked}
            className={`clc-btn ${props.cls}`}>
            {props.children}
        </button>
    );
};

export default Button;
