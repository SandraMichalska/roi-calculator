import React from 'react';

import './Progress.scss';

const Progress = props => (
    <div>
        <div className='clc-form-progress'>
            <div
                className='clc-form-progress__step'
                style={{ width: `${props.progress}%` }}
            />
        </div>
    </div>
);

export default Progress;
