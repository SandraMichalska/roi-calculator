import React from 'react';
import ReactTooltip from 'react-tooltip';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Tooltip.scss';

const Tooltip = props => (
    <span className='clc-tooltip' data-tip={props.tip}>
        <FontAwesomeIcon icon='question-circle' />
        <ReactTooltip place={props.tooltipPlace} />
    </span>
);

export default Tooltip;
