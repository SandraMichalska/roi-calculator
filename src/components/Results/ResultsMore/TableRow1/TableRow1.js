import React from 'react';
import Tooltip from '../../../UI/Tooltip/Tooltip';

const TableRow1 = props => (
    <div>
        {props.rowTxt}
        <Tooltip tip={props.tip} tooltipPlace='right' />
    </div>
);

export default TableRow1;
