import React from 'react';
import ReactTable from 'react-table';

import StartAgain from '../../UI/StartAgain/StartAgain';
import Back from '../../UI/Back/Back';
import TableRow1 from './TableRow1/TableRow1';
import './ResultsMore.scss';

const ResultsMore = props => {
    // just some made up values
    const tableData = [
        {
            row: (
                <TableRow1
                    rowTxt='Site development'
                    tip='For any event, you will need a website to direct your traffic to. If you already have one, you need a dedicated section for managing your live pay-per-views. Such sites must be using responsive design so they work on all devices – computers, phones, tablets and smart TVs. Then comes the mandatory functions including a 1-click registration process, all social media integrations and couponing & promotional solution system, not to mention translations and popular payment methods for your users. All of this has been properly tested.'
                />
            ),
            value1: 14000,
            value2: 10000,
        },
        {
            row: (
                <TableRow1
                    rowTxt='Site maintenance'
                    tip='Once set up, the site needs to be configured, tested and optimized prior, during and after the live event so that your viewers can transact super fast. Our SaaS platform spares you from all the costs associated with a domain, hosting and databases. We deal with the load testing, SSL acceleration, high peak traffic increase and high velocity transactions. Nothing needs to be done on your side.'
                />
            ),
            value1: 14000,
            value2: 10000,
        },
        {
            row: (
                <TableRow1
                    rowTxt='Recording & production costs'
                    tip='These are your typical fixed costs for organizing a live stream: capturing the action live – having the cameras, sound, and everything properly mastered at your location. You also need to invest in the promotion of your event: create press awareness, use social media and partner with media outlets. The more you communicate with your customers the better.'
                />
            ),
            value1: 14000,
            value2: 10000,
        },
        {
            row: (
                <TableRow1
                    rowTxt='Live streaming fees'
                    tip='You as the publisher need to set up your own broadcast and streaming accounts. Based on your quality expectations and global footprint for the event, there are many solutions available at different prices from top Online Video Platforms, for example Brightcove, Ooyala, Livestream, IBM, Dailymotion and many others, to secure a seamless and faultless streaming experience. Our team can assist if you need advise.'
                />
            ),
            value1: 14000,
            value2: 10000,
        },
        {
            row: (
                <TableRow1
                    rowTxt='Expected quality'
                    tip='A few minutes ago, we asked you to specify the level of quality you expect (Entry/Mid or High). Such selection had an impact on the fixed costs above and also on the conversion rate (the higher quality of the experience, the higher the conversion rate).'
                />
            ),
            value1: 'mid',
            value2: 'mid',
        },
        {
            row: 'Total fixed costs',
            value1: 14000,
            value2: 10000,
        },
        {
            row: '',
            value1: '',
            value2: '',
        },
        {
            row: (
                <TableRow1
                    rowTxt='Traffic required'
                    tip='Projected number of unique visitors that will arrive on the live event page (taking into account your Facebook followers, YouTube subscribers to best estimate your total traffic). The conversion rate will be based on this number to calculate the number of tickets sold: this is the ratio of people who successfully bought a ticket.'
                />
            ),
            value1: 60,
            value2: 60,
        },
        {
            row: (
                <TableRow1
                    rowTxt='Conversion rate'
                    tip="Several factors can greatly influence your conversion rate. From a truly responsive design that is easy embed to your website and CRM tools, to fully featured landing page, we have mastered all of these criteria’s to help you convert more visitors into customers. Nowadays 20% of our customers prefer to stream events on their mobiles and 10% on tablets. We're fully responsive design will bring a significant impact on your conversions. People are always connected to their social media account and do not want to remember their password, that is why up to 35% of viewers prefer to log in this way. Optimized Social sign-on and optimized user authentication ease content access and conversions!"
                />
            ),
            value1: '3%',
            value2: '5%',
        },
    ];

    const tableColumns = [
        { Header: '', accessor: 'row' },
        { Header: 'Home Grown', accessor: 'value1' },
        { Header: 'Our company', accessor: 'value2' },
    ];

    return (
        <div className='clc-results-more'>
            <p>Based on your entries we did the math for you:</p>

            <ReactTable
                className='clc-results-more__table'
                data={tableData}
                columns={tableColumns}
                showPagination={false}
                defaultPageSize={9}
                sortable={false}
            />

            <div className='clc-results-more__btn-wrap'>
                <StartAgain />
                <Back history={props.history} cls='' />
            </div>
        </div>
    );
};

export default ResultsMore;
