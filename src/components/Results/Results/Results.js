import React from 'react';
import ReactTable from 'react-table';
import { Link } from 'react-router-dom';

import Heading from '../../UI/Heading/Heading';
import Button from '../../UI/Button/Button';
import StartAgain from '../../UI/StartAgain/StartAgain';
import Back from '../../UI/Back/Back';
import './Results.scss';

class Results extends React.Component {
    state = {
        formData: null,
        loading: false,
        error: null,
    };

    componentDidMount() {
        this.setState({ loading: true });

        fetch(
            'https://live-ppv-event-roi-calculator.firebaseio.com/form-data.json'
        )
            .then(response => {
                response.json().then(response => {
                    const lastResult =
                        response[
                            Object.keys(response)[
                                Object.keys(response).length - 1
                            ]
                        ];
                    this.setState({ formData: lastResult, loading: false });
                });
            })
            .catch(err => {
                console.log('Fetch error:', err);
                this.setState({ error: err, loading: false });
            });
    }

    prepareTable = () => {
        // just some made up calculations
        const ticketNumber = this.state.formData.ticketNumber,
            ticketPrice = this.state.formData.ticketPrice,
            streamingQuality = this.state.formData.streamingQuality;

        let streamingQualityPrice;

        if (streamingQuality === 'entry') {
            streamingQualityPrice = 100;
        } else if (streamingQuality === 'mid') {
            streamingQualityPrice = 200;
        } else if (streamingQuality === 'high') {
            streamingQualityPrice = 300;
        }

        const revenues = [
            ticketNumber * ticketPrice,
            ticketNumber * ticketPrice + 100,
        ];
        const costs = [
            ticketNumber * ticketPrice + streamingQualityPrice,
            ticketNumber * ticketPrice + streamingQualityPrice / 2,
        ];

        this.tableData = [
            { row: 'Revenues', value1: revenues[0], value2: revenues[1] },
            { row: 'Costs', value1: costs[0], value2: costs[1] },
            {
                row: 'Profits',
                value1: revenues[0] - costs[0],
                value2: revenues[1] - costs[1],
            },
        ];

        this.tableColumns = [
            { Header: '', accessor: 'row' },
            { Header: 'Home Grown', accessor: 'value1' },
            { Header: 'Our company', accessor: 'value2' },
        ];
    };

    handleMoreResults = () => {
        this.props.history.push('/results-more');
    };

    render() {
        if (this.state.loading) {
            return (
                <div className='clc-results'>
                    <p>Please wait a second...</p>
                </div>
            );
        }

        if (this.state.error) {
            return (
                <div className='clc-results'>
                    <p className='clc-results__error'>
                        An error occurred. Please try again later.
                    </p>
                    <Link to='/' className='clc-results__error-link'>
                        Go to the main page
                    </Link>
                </div>
            );
        }

        if (!this.state.formData) return null;

        this.prepareTable();

        return (
            <div className='clc-results'>
                <p className='clc-results__heading'>
                    <Heading />
                </p>
                <p className='clc-results__txt'>
                    Improve your results with us!
                </p>

                <ReactTable
                    className='clc-results__table'
                    data={this.tableData}
                    columns={this.tableColumns}
                    showPagination={false}
                    defaultPageSize={3}
                    sortable={false}
                />

                <p>
                    Do you want to understand the logic behind this calculation?
                    Grab your results and we will explain you everything about
                    conversion rates, customer care and operation fees.
                </p>
                <div className='clc-results__btn-wrap'>
                    <Button
                        type='button'
                        cls='clc-results__btn'
                        clicked={this.handleMoreResults}>
                        Find out now
                    </Button>
                    <StartAgain />
                    <Back history={this.props.history} cls='' />
                </div>
            </div>
        );
    }
}

export default Results;
