import React from 'react';

import Heading from '../UI/Heading/Heading';
import Button from '../UI/Button/Button';
import './Welcome.scss';

class Welcome extends React.Component {
    handleContinue = () => {
        this.props.history.push('/form/1');
    };

    render() {
        return (
            <div className='clc-welcome'>
                <h1 className='clc-welcome__heading'>
                    <Heading />
                </h1>
                <div className='clc-welcome__txt'>
                    Ever wondered{' '}
                    <strong>how much money you can generate</strong> with the
                    Live Pay-Per-View model? Based on our experience in{' '}
                    <strong>managing over 10,000 PPV events</strong> per year,
                    we created this unique ROI calculator for you.
                </div>

                <Button
                    type='button'
                    cls='clc-welcome__btn'
                    clicked={this.handleContinue}>
                    Give it a try!
                </Button>
            </div>
        );
    }
}

export default Welcome;
