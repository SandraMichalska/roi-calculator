import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Heading from '../UI/Heading/Heading';
import Button from '../UI/Button/Button';
import Progress from '../UI/Progress/Progress';
import './FormStep.scss';

const FormStep = props => (
    <div className='clc-form'>
        <p className='clc-form__heading-txt'>
            <Heading />
        </p>
        <p>{props.stepsData.txt1}</p>
        <div className='clc-form__icon'>
            <FontAwesomeIcon icon={props.stepsData.icon} />
        </div>
        <p>{props.stepsData.txt2}</p>
        <div className='clc-form__question'>
            {props.stepsData.question}
            {props.stepsData.tooltip}
        </div>
        {props.stepsData.input}
        <div
            className={`clc-form__btn-wrap ${
                props.stepsData.back ? '' : 'clc-form__btn-wrap--right'
            }`}>
            {props.stepsData.back}
            <Button
                type={props.stepsData.buttonType}
                isFormValid={props.stepsData.isFormValid}
                cls='clc-form__btn'
                clicked={props.onContinue}>
                {props.stepsData.buttonValue}
                <div className='clc-form__btn-arrow'>
                    <span className='clc-form__btn-arrow-inner' />
                </div>
            </Button>
        </div>
        <Progress progress={props.stepsData.progress} />
    </div>
);

export default FormStep;
