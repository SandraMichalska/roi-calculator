const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// const ImageminPlugin = require("imagemin-webpack");
// const imageminGifsicle = require("imagemin-gifsicle");
// const imageminJpegtran = require("imagemin-jpegtran");
// const imageminOptipng = require("imagemin-optipng");
// const imageminSvgo = require("imagemin-svgo");
const PrettierPlugin = require("prettier-webpack-plugin");
const path = require('path');

module.exports = {
    entry: {
        bundle: ['./src/index.js']
        // style: ['./index.scss'] // was needed for extract-text-webpack-plugin
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js',
        publicPath: ''
    },
    devServer: {
        historyApiFallback: true
    },
    devtool: 'eval-source-map', // comment for production?
    module: {
        rules: [
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader', 
                    MiniCssExtractPlugin.loader, 
                    'css-loader', 
                    'sass-loader',
                    {
                        loader: "postcss-loader",
                        options: {
                            ident: 'postcss',
                            plugins: [
                                require('autoprefixer')({
                                    'browsers': ['> 1%', 'last 2 versions']
                                }),
                            ]
                        }
                    },
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                '@babel/preset-env', 
                                '@babel/react',
                                {
                                    plugins: [
                                    '@babel/plugin-proposal-class-properties'
                                    ]
                                },
                            ],
                        },
                    },
                    'eslint-loader',
                ]
            },
            // {
            //     test: /\.(jpg|jpeg|png|gif|svg)$/i,
            //     use: [
            //         {
            //             loader: "file-loader",
            //             options: {
            //                 emitFile: true, // Don't forget emit images
            //                 name: "[path][name].[ext]"
            //             },
            //         },
            //     ],
            // },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: "[path][name].[ext]"
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/index.html'),
            filename: 'index.html',
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            filename: 'index.css'
        }),
        new CopyWebpackPlugin([
            {
              from: 'src/static/img', to: 'src/static/img'
            },
        ]),
        new PrettierPlugin({
            code: 100,
            tabWidth: 4,
            useTabs: false,
            semi: true,
            singleQuote: true,
            jsxSingleQuote: true,
            jsxBracketSameLine: true,
            trailingComma: "es5",
            bracketSpacing: true
        })
        // new ImageminPlugin({
        //     bail: false, // Ignore errors on corrupted images
        //     cache: true,
        //     name: "[path][name].[ext]",
        //     imageminOptions: {
        //         // Lossless optimization with custom option
        //         // Feel free to experement with options for better result for you
        //         plugins: [
        //             imageminGifsicle({
        //                 interlaced: true
        //             }),
        //             imageminJpegtran({
        //                 progressive: true
        //             }),
        //             imageminOptipng({
        //                 optimizationLevel: 5
        //             }),
        //             imageminSvgo({
        //                 removeViewBox: true
        //             })
        //         ]
        //     }
        // })
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
}